import os
import random
import numpy as np
import pandas as pd
import importlib as imp
import matplotlib.pyplot as plt
from tqdm import tqdm, tqdm_notebook
import warnings
warnings.simplefilter('ignore')
pd.options.display.max_columns = 100

from utils import tools, visualize, common
bname = 'benchmarks/B/B-n35-k5.vrp'
problem = tools.get_problem(bname)



tools     = imp.reload(tools)
visualize = imp.reload(visualize)
common    = imp.reload(common)

visualize.visualize_problem(problem)
for k, v in problem.items():
    if k !='locations' and k !='dists':
        print(k,"->", v)
plt.savefig('output/images/problem'+bname[-10:]+'.png', dpi=400)

common   = imp.reload(common)
solution = common.generate_solution(problem, alpha=0.01, betta=50, verbose=False)
print('Is feasible? {}'.format(common.check_solution(problem, solution, verbose=True)))
sol_cost = common.compute_solution(problem, solution)
print('Solution cost:', sol_cost)
solution



from algorithm import bee_colony, local_search, neighbor_operator
bee_colony = imp.reload(bee_colony)
local_search = imp.reload(local_search)
neighbor_operator = imp.reload(neighbor_operator)
common = imp.reload(common)


ABC = bee_colony.BeeColony(problem)
ABC.set_params(
                   n_epoch=200,
                   n_initials=30,
                   n_onlookers=20,
                   search_limit=50
              )
abc_solution = ABC.solve(alpha=0.1, delta=0.01, gen_alpha=0.01, gen_betta=25)



new_cost = common.compute_solution(problem, abc_solution)
print('Is feasible?',common.check_solution(problem, abc_solution, verbose=True))
print('Is capacity?',common.check_capacity_criteria(problem, abc_solution, verbose=False))
print('ABC cost: {}'.format(new_cost))
print('Optimal cost: {}'.format(problem['optimal']))



visualize = imp.reload(visualize)
visualize.visualize_fitness(ABC.history)
plt.savefig('output/images/history_'+bname[-10:]+'.png', dpi=400)



#plt.figure(figsize=(9,4))
#plt.plot(ABC.history_alpha, label='alpha')
plt.legend()
plt.grid()
plt.show()

visualize.visualize_problem(problem, solution, figsize=(13,6))
plt.savefig('output/images/beforeABC_'+bname[-10:]+'.png', dpi=400)



visualize.visualize_problem(problem, abc_solution, figsize=(13,6))
depots = list(filter(lambda i: abc_solution[i]==0, range(len(abc_solution))))
for i, route in  enumerate(common.get_routes(abc_solution)):
    print('Route #{}'.format(i+1),route)
# len(abc_solution), len(np.unique(abc_solution))
plt.savefig('output/images/afterABC_'+bname[-10:]+'.png', dpi=400)
